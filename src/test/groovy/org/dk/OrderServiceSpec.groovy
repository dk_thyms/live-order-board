package org.dk

import org.dk.models.Order
import org.dk.models.OrderSummary
import org.dk.services.OrderService
import org.dk.types.OrderType;
import spock.lang.Specification

class OrderServiceSpec extends Specification {
  private OrderService orderService

  def setup() {
    orderService = new OrderService()
  }

  def "should return Sell Orders for single orders"() {
    given:
    Order order = new Order("1", 306, 3.5d, OrderType.SELL)
    orderService.add(order)

    when:
    List<OrderSummary> orderSummaries = orderService.getSellOrders()

    then:
    OrderSummary orderSummary = orderSummaries.get(0);
    orderSummary.getPrice() == 306
    orderSummary.getQuantity() == 3.5d
  }

  def "should return Sell Orders for multiple orders"() {
    given:
    Order order1 = new Order("1", 306, 3.5d, OrderType.SELL)
    Order order2 = new Order("1", 310, 1.2d, OrderType.SELL)
    Order order3 = new Order("2", 307, 1.5d, OrderType.SELL)
    Order order4 = new Order("3", 306, 2.0d, OrderType.SELL)

    and:
    orderService.add(order1)
    orderService.add(order2)
    orderService.add(order3)
    orderService.add(order4)

    when:
    List<OrderSummary> orderSummaries = orderService.getSellOrders()

    then:
    OrderSummary orderSummary1 = orderSummaries.get(0)
    orderSummary1.getPrice() == 306
    orderSummary1.getQuantity() == 5.5d

    and:
    OrderSummary orderSummary2 = orderSummaries.get(1)
    orderSummary2.getPrice() == 307
    orderSummary2.getQuantity() == 1.5d

    and:
    OrderSummary orderSummary3 = orderSummaries.get(2)
    orderSummary3.getPrice() == 310
    orderSummary3.getQuantity() == 1.2d
  }

  def "should return Buy Orders for a single order"() {
    given:
    Order order = new Order("1", 306, 2.5d, OrderType.BUY)
    orderService.add(order)

    when:
    List<OrderSummary> buyOrders = orderService.getBuyOrders()

    then:
    OrderSummary orderSummary = buyOrders.get(0);
    orderSummary.getPrice() == 306
    orderSummary.getQuantity() == 2.5d
  }

  def "should return Buy Orders for multiple orders"() {
    given:
    Order order1 = new Order("1", 306, 2.5d, OrderType.BUY)
    Order order2 = new Order("1", 310, 1.5d, OrderType.BUY)
    Order order3 = new Order("2", 302, 1.2d, OrderType.BUY)
    Order order4 = new Order("3", 306, 1.5d, OrderType.BUY)

    and:
    orderService.add(order1)
    orderService.add(order2)
    orderService.add(order3)
    orderService.add(order4)

    when:
    def orderSummaries = orderService.getBuyOrders()

    then:
    OrderSummary orderSummary1 = orderSummaries.get(0)
    orderSummary1.getPrice() == 310
    orderSummary1.getQuantity() == 1.5d

    and:
    OrderSummary orderSummary2 = orderSummaries.get(1)
    orderSummary2.getPrice() == 306
    orderSummary2.getQuantity() == 4.0d

    and:
    OrderSummary orderSummary3 = orderSummaries.get(2)
    orderSummary3.getPrice() == 302
    orderSummary3.getQuantity() == 1.2d
  }

  def "should return Sell and Buy Orders for a mixed type of orders"() {
    given:
    Order order1 = new Order("1", 306, 2.5d, OrderType.BUY)
    Order order2 = new Order("1", 310, 1.5d, OrderType.BUY)
    Order order3 = new Order("3", 306, 1.5d, OrderType.BUY)

    and:
    Order order4 = new Order("2", 306, 1.2d, OrderType.SELL)
    Order order5 = new Order("2", 310, 2.5d, OrderType.SELL)
    Order order6 = new Order("1", 306, 1.5d, OrderType.SELL)

    and:
    orderService.add(order1)
    orderService.add(order2)
    orderService.add(order3)
    orderService.add(order4)
    orderService.add(order5)
    orderService.add(order6)

    when:
    def buyOrders = orderService.getBuyOrders()

    then:
    OrderSummary buySummary1 = buyOrders.get(0)
    buySummary1.getPrice() == 310
    buySummary1.getQuantity() == 1.5d

    and:
    OrderSummary buySummary2 = buyOrders.get(1)
    buySummary2.getPrice() == 306
    buySummary2.getQuantity() == 4.0d

    when:
    def sellOrders = orderService.getSellOrders()

    then:
    OrderSummary sellSummary2 = sellOrders.get(0)
    sellSummary2.getPrice() == 306
    sellSummary2.getQuantity() == 2.7d

    and:
    OrderSummary sellSummary1 = sellOrders.get(1)
    sellSummary1.getPrice() == 310
    sellSummary1.getQuantity() == 2.5d
  }

  def "should be able to cancel an order"() {
    given:
    Order order1 = new Order("1", 306, 2.5d, OrderType.BUY)
    Order order2 = new Order("1", 310, 1.5d, OrderType.BUY)
    Order order3 = new Order("2", 302, 1.2d, OrderType.BUY)
    Order order4 = new Order("3", 306, 1.5d, OrderType.BUY)

    and:
    orderService.add(order1)
    orderService.add(order2)
    orderService.add(order3)
    orderService.add(order4)

    when:
    def orderSummaries = orderService.getBuyOrders()

    then:
    orderSummaries.size() == 3

    and:
    OrderSummary orderSummary1 = orderSummaries.get(0)
    orderSummary1.getPrice() == 310
    orderSummary1.getQuantity() == 1.5d

    when:
    orderService.cancel(order2)

    and:
    orderSummaries = orderService.getBuyOrders()

    then:
    orderSummaries.size() == 2

    and:
    OrderSummary orderSummary4 = orderSummaries.get(0)
    orderSummary4.getPrice() == 306
    orderSummary4.getQuantity() == 4.0d
  }
}