package org.dk.models;

public class OrderSummary {

	private int price;
	private double quantity;

	public OrderSummary(int price, double quantity) {
		this.price = price;
		this.quantity = quantity;
	}

	public int getPrice() {
		return this.price;
	}

	public double getQuantity() {
		return quantity;
	}
}