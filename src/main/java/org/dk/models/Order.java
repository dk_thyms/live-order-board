package org.dk.models;

import org.dk.types.OrderType;

public class Order {
	private String id;
	private int price;
	private double quantity;
	private OrderType orderType;

	public Order(String id, int price, double quantity, OrderType orderType) {
		this.id = id;
		this.price = price;
		this.quantity = quantity;
		this.orderType = orderType;
	}

	public int getPrice() {
		return price;
	}

	public double getQuantity() {
		return quantity;
	}

	public OrderType getType() {
		return orderType;
	}
}