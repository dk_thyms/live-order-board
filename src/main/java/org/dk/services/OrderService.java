package org.dk.services;

import org.dk.models.Order;
import org.dk.models.OrderSummary;
import org.dk.types.OrderType;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.*;

public class OrderService {
  private final List<Order> orders;

  public OrderService() {
    this.orders = new ArrayList<>();
  }

  public void add(Order order) {
    this.orders.add(order);
  }

  public List<OrderSummary> getSellOrders() {
    List<OrderSummary> orderSummaries;

    orderSummaries = orders.stream()
      .filter(order -> order.getType() == OrderType.SELL)
      .collect(groupingBy(Order::getPrice, summingDouble(Order::getQuantity)))
      .entrySet().stream().map((entry) -> new OrderSummary(entry.getKey(), entry.getValue()))
      .sorted((order1, order2) -> order1.getPrice() - order2.getPrice())
      .collect(toList());

    return orderSummaries;
  }

  public List<OrderSummary> getBuyOrders() {
    List<OrderSummary> orderSummaries;

    orderSummaries = orders.stream()
      .filter(order -> order.getType() == OrderType.BUY)
      .collect(groupingBy(Order::getPrice, summingDouble(Order::getQuantity)))
      .entrySet().stream().map((entry) -> new OrderSummary(entry.getKey(), entry.getValue()))
      .sorted((order1, order2) -> order2.getPrice() - order1.getPrice())
      .collect(toList());

    return orderSummaries;
  }

  public void cancel(Order order) {
    orders.remove(order);  // could be a better implementation if each order had an ID, so that cancellation/removal could have been based on that ID
  }
}